import React from "react";
import "../assets/styles/Dashboard.css";
import Footer from "../components/Footer";
import Main from "../components/Main";
import Nav from "../components/Nav";
import Sidebar from "../components/Sidebar";

const Dashboard = () => {
  return (
    <div>
      <Nav />
      <div className="flex overflow-hidden bg-white pt-16">
        <Sidebar />
        <div
          className="bg-gray-900 opacity-50 hidden fixed inset-0 z-10"
          id="sidebarBackdrop"
        ></div>
        <div
          id="main-content"
          className="h-full w-full bg-gray-50 relative overflow-y-auto lg:ml-64"
        >
          <Main />

          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
